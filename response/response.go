package response

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
)

// JSONHeader is the header for JSON responses
const JSONHeader = "application/json; charset=UTF-8"

// StandardResponse is the JSON response from an API method, indicating success or failure.
type StandardResponse struct {
	Success bool   `json:"success"`
	Code    string `json:"code"`
	Message string `json:"message"`
}

// RecordsResponse is the JSON response from an API method for lists
type RecordsResponse struct {
	StandardResponse
	Records interface{} `json:"records"`
}

// FormatStandardResponse returns a JSON response from an API method, indicating success or failure
func FormatStandardResponse(success bool, code, message string, w http.ResponseWriter) error {
	w.Header().Set("Content-Type", JSONHeader)
	response := StandardResponse{Success: success, Code: code, Message: message}

	if !response.Success {
		w.WriteHeader(http.StatusBadRequest)
	}

	// Encode the response as JSON
	return encodeResponse(w, response)
}

// FormatListResponse returns a JSON response from an API method for lists
func FormatListResponse(records interface{}, w http.ResponseWriter) error {
	response := RecordsResponse{StandardResponse{Success: true}, records}

	// Encode the response as JSON
	return encodeResponse(w, response)
}

func encodeResponse(w http.ResponseWriter, response interface{}) error {
	// Encode the response as JSON
	if err := json.NewEncoder(w).Encode(response); err != nil {
		log.Println("Error forming the response.")
		return err
	}
	return nil
}

// ParseRecordsResponse decodes the records response
func ParseRecordsResponse(r io.Reader) (RecordsResponse, error) {
	// Parse the response
	result := RecordsResponse{}
	err := json.NewDecoder(r).Decode(&result)
	return result, err
}

// ParseStandardResponse decodes the standard response
func ParseStandardResponse(r io.Reader) (StandardResponse, error) {
	// Parse the response
	result := StandardResponse{}
	err := json.NewDecoder(r).Decode(&result)
	return result, err
}
